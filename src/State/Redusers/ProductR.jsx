import {
    ACSESS_ORDER,
    CART_ITEMS,
    CART_ITEMS_CHANGE_QTY,
    CART_ITEMS_REMOVE,
    DETAIL_PRODUCT,
    CART_ITEMS_CHANGE_MINUS
} from "../../type/type";


let initialState = {
    cartItems: [],
    order: [],
    detailProduct: []
};
const AcsessOrder = (state = initialState, action) => {
    switch (action.type) {
        case CART_ITEMS:
            return {...state, cartItems: [...state.cartItems, action.product]}
        case CART_ITEMS_REMOVE:
            return {...state, cartItems: [...state.cartItems.filter((x) => x._id !== action.product._id)]}

        case CART_ITEMS_CHANGE_QTY:
            return {
                ...state, cartItems: [...state.cartItems.map(x => {
                    if (x._id === action.product._id) {
                        return {...x, qty: Number(action.product.qty) + Number(action.qty)}
                    }
                    return x
                })]
            }
        case CART_ITEMS_CHANGE_MINUS:
            return {
                ...state, cartItems: [...state.cartItems.map((x) => {
                    if (x._id === action.product._id) {
                        return {...x, qty: Number(action.product.qty) - 1}
                    }
                    return x
                })]
            }
        case ACSESS_ORDER:
            return {...state, order: action.order}

        case DETAIL_PRODUCT:
            return {...state, detailProduct: action.detailProduct}
        default:
            return state

    }
}
export default AcsessOrder