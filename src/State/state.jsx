import {createStore, combineReducers, applyMiddleware} from 'redux'
import AcsessOrder from "./Redusers/ProductR";
import thunkMiddleware from'redux-thunk'



// Функция Redux которая объеденяет все редюсеры, каждый редюсер это объект
let reducersGroup = combineReducers({
    AcsessOrder:AcsessOrder,
});

const store = createStore(reducersGroup, applyMiddleware(thunkMiddleware));


export default store
