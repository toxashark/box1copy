import {BrowserRouter} from 'react-router-dom'

import './App.css';
import MainPageRoute from "./Components/MainPages/MainPageRoute";




function App() {
  return (

          <BrowserRouter>
              <div className="App">
                  <MainPageRoute/>
              </div>
          </BrowserRouter>


  );
}

export default App;
