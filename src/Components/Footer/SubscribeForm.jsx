import {ErrorMessage, Field, Form, Formik, getIn} from "formik";
import React, {useState} from "react";
import * as Yup from "yup";
import SuccessSnackBar from "../assets/SnackBar/Success";

const SubscribeForm = () =>{
    const [activeSnack, setActiveSnack] = useState(false)
    const getStyles =(errors, fieldName) => {
        if (getIn(errors, fieldName)) {
            return {
                border: '1px solid red'
            }
        }
    }
    const validationSchema = Yup.object({
        email: Yup.string().required(' '),

    })
    const initialValues = {
        email: ''
    }
    const onSubmit = (values, onSubmitProps) => {
        onSubmitProps.resetForm()
        setActiveSnack(true)
        console.log(values)
    }
    return (
        <div>
            <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                {formik => {
                    return (
                        <Form>
                            <div>
                                <div className="footer_subscribe_form">
                                    <label htmlFor="email"></label>
                                    <Field style={getStyles(formik.errors, 'email')}
                                        type="email"
                                        id="email"
                                        name="email"
                                        placeholder='Your email adress'
                                    />
                                    <ErrorMessage name='email'/>
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </Form>
                    )
                }}
            </Formik>
            <SuccessSnackBar activeSnack={activeSnack} setActiveSnack={setActiveSnack}>
                <p>Success</p>
            </SuccessSnackBar>
        </div>
    )
}
export default SubscribeForm