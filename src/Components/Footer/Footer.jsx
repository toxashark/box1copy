import React from 'react';
import {FiInstagram} from 'react-icons/fi';
import {FaFacebookF} from 'react-icons/fa';
import {AiOutlineGooglePlus} from 'react-icons/ai';
import ImagesMarat from '../Header/icon/logo.png'
import SubscribeForm from "./SubscribeForm";


const Footer = () => {


    return (
        <footer>
            <div className="main-footer-container">
                <div className="subscriber-container">
                    <div className="subscriber_title">
                        <p>SUBSCRIBE</p>
                        <p id="blue">Weekly newsletter</p>
                    </div>
                    <SubscribeForm/>
                </div>
                <div className="footer-container">
                    <div className="footer-content-img">
                        <img src={ImagesMarat} alt="Marat"/>
                    </div>
                    <div className="footer-content-text">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                        Vivamus tristique ligula quis orci malesuada tincidunt.
                    </div>
                    <div className="footer-social-icon">
                        <ul>
                            <li><a href="!#"><FiInstagram/></a></li>
                            <li><a href="!#"><FaFacebookF/></a></li>
                            <li><a href="!#"><AiOutlineGooglePlus/></a></li>
                        </ul>
                    </div>
                </div>
                <div className="footer_company">
                    <a target="_blank" rel='noreferrer' href="https://themarat.com/en/website-development.html">© Copyright 2021 The Marat Сompany Inc.
                        (MARAT™). All Rights Reserved.</a>
                </div>
            </div>
        </footer>
    );
};

export default Footer;