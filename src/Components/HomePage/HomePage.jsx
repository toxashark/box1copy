import React from 'react';
import Header from "../Header/Header";
import Slider from "../Slider/slider1";
import NavBar from "../MainPages/NavBar/NavBar";
import Products from "../MainPages/Products/Products";
import AboutUs from "../MainPages/AboutUs/AboutUs";
import ServiceOrder from "../MainPages/ServiceOrder/ServiceOrder";
import Footer from "../Footer/Footer";

const HomePage = ({dataProduct}) => {
    return (
        <div>
            <Header/>
            <Slider/>
            <div className='MainPage-container'>
                {window.innerWidth < 999 ? null : <NavBar/>}
                <Products dataProduct={dataProduct}/>
                {window.innerWidth > 999 ? null : <NavBar/>}
            </div>
            <AboutUs/>
            <ServiceOrder/>
            <Footer/>
        </div>
    );
};

export default HomePage;