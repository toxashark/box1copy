import React from 'react';


import ImageService from './images/delivery-dark.webp'
import { ImCheckmark } from 'react-icons/im';
import Serviceform from "./Serviceform";




const ServiceOrder = () => {
    return (
        <>
            <div className="main-container-service" id="ServiceOrder">
                <div className="service-container">
                </div>
                <div className="service-container-content">
                    <div className="service-images">
                        <img src={ImageService} alt=""/>
                    </div>
                    <div className="service-container-text">
                        <div className="service-content-title">
                            <span>Delivery</span> Service
                        </div>
                        <div className="service-content-text">
                            Our delivery service employs more than 100 professional couriers. We will deliver water to
                            your
                            home for 1 hour to anywhere in the city.
                        </div>
                        <div className="service-content-timetable">
                            <ul>
                                <li><ImCheckmark/>Free Delivery Service</li>
                                <li><ImCheckmark/>7 days a week</li>
                                <li><ImCheckmark/>8:00 – 22:00 Every day</li>
                            </ul>
                        </div>
                        <div className="service-button">
                            <a href="!#">Read More</a>
                        </div>
                    </div>
                   <Serviceform/>
                </div>
            </div>
        </>
    );
};

export default ServiceOrder;