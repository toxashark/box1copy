import * as Yup from "yup";
import {ErrorMessage, Field, Form, Formik, getIn} from "formik";
import React, {useState} from "react";
import SuccessSnackBar from "../../assets/SnackBar/Success";



const Serviceform = () =>{

    const [activeSnack, setActiveSnack] = useState(false)

    const getStyles =(errors, fieldName) => {
        if (getIn(errors, fieldName)) {
            return {
                border: '1px solid red'
            }
        }
    }

    const StyleRequired = (props) => {
        return (
            <>
                <div className="ErrorMassage">
                    {props.children}
                </div>
            </>

        )
    }
    const initialValues = {
        name: '',
        phone: '',
        adres: '',
        number:''
    }

    const onSubmit = (values, onSubmitProps) => {
        onSubmitProps.resetForm()
        setActiveSnack(true)
        console.log(values)
    }
    const validationSchema = Yup.object({
        name: Yup.string().required(' '),
        phone: Yup.string().required(' '),
        adres: Yup.string().required(' '),
        number: Yup.string().required(' '),
    })
    return (
        <div className="service-form">
            <Formik validateOnBlur={false} validateOnChange={false} initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                {formik => {
                    return (
                        <Form>
                            <div className="service-form-container">
                                <div className="service-title-order">
                                    <p>Please, fill delivery form</p>
                                </div>
                                <div className="service-input-name-phone">
                                    <label htmlFor="name"></label>
                                    <Field style={getStyles(formik.errors, 'name')}
                                           type="name"
                                           id="name"
                                           name="name"
                                           placeholder='Your name'
                                    />
                                    <ErrorMessage name='name' component={StyleRequired}/>
                                </div>
                                <div className="service-input-name-phone">
                                    <label htmlFor="phonenumber"></label>
                                    <Field style={getStyles(formik.errors, 'phone')}
                                           type="phone"
                                           id="phone"
                                           name="phone"
                                           placeholder='Phone Number'
                                    />
                                    <ErrorMessage name='phone' component={StyleRequired}/>
                                </div>
                                <div className="service-input-adress-number">
                                    <label htmlFor="name"></label>
                                    <Field style={getStyles(formik.errors, 'adres')}
                                           type="adres"
                                           id="adres"
                                           name="adres"
                                           placeholder='Adress'
                                    />
                                    <ErrorMessage name='adres' component={StyleRequired}/>
                                    <Field style={getStyles(formik.errors, 'number')}
                                           type="number"
                                           id="number"
                                           name="number"
                                           placeholder='number'
                                    />
                                    <ErrorMessage name='number' component={StyleRequired}/>
                                </div>
                                <button type="submit">Make Order</button>
                            </div>
                        </Form>
                    )
                }}
            </Formik>
            <SuccessSnackBar activeSnack={activeSnack} setActiveSnack={setActiveSnack}>
                <div>
                    <p>Success</p>
                </div>
            </SuccessSnackBar>
        </div>
    )
}
export default Serviceform