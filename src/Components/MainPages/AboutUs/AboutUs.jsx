import React from 'react';
import ImagesAbout from './images/waterfall-1618434_1920.webp'



const AboutUs = () => {
    return (
        <div className="aboutus-Container" id='AboutUs'>
            <div className="aboutUs-titl">
                <div className="small-titl-aboutUs">
                    <p>Water skills</p>
                </div>
                <p id='main-title-aboutUs'>About Aquaterias</p>
            </div>
            <div className="container-aboutUs">
                <div>
                    <div className="content-text-aboutUs">
                        <div className="content-title-aboutUs">
                            <p>Our company was founded in 1965</p>
                        </div>
                        <div className="aboutUs-text">
                            <p>Aquatiras is ideal for drinking, cooking, sports and even for children. The product is
                                certified in 12 countries.</p>
                        </div>
                        <div className="aboutUs-main-text">
                            Sed viverra, lorem in maximus faucibus, odio libero fringilla dolor, convallis vestibulum
                            risus
                            nisi ac neque. Maecenas convallis ligula metus, ac viverra magna egestas mollis. Etiam sed
                            tortor vel purus aliquam faucibus. Pellentesque vel nisi pharetra, euismod sapien et,
                            volutpat
                            nulla. In congue maximus malesuada.
                        </div>
                        <a href="!#">read more</a>
                    </div>
                </div>
                <div className="main-images-aboutUs">
                    <img src={ImagesAbout} alt="WaterImages"/>
                </div>
            </div>
        </div>
    );
};

export default AboutUs;