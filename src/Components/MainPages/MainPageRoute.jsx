import React from 'react';
import {Switch, Route} from "react-router-dom";


import data from '../../data.json'
import PagesProduct from "../PagesProduct/PagesProduct";
import HomePage from "../HomePage/HomePage";

const Cart = React.lazy(() => import('../Cart/cart'));
const Checkout = React.lazy(() => import('../Checkout/checkout'));


const MainPageRoute = () => {
    const dataProduct = data
    return (
        <Switch>
            <Route exact path='/' render={()=><HomePage dataProduct={dataProduct}/>}/>
            <Route exact path='/product/:id' render={() => <PagesProduct/>}/>
            <Route exact path='/cart' render={() => {
                return (
                    <React.Suspense fallback={<div>Загрузка...</div>}>
                        <Cart/>
                    </React.Suspense>
                )
            }}/>
            <Route exact path='/checkout' render={() => {
                return (
                    <React.Suspense fallback={<div>Загрузка...</div>}>
                        <Checkout/>
                    </React.Suspense>
                )
            }}/>
        </Switch>
    );
};
export default MainPageRoute

