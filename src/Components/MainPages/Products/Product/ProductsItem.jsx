import React, {useState} from 'react';
import {BiCart} from 'react-icons/bi';
import {ImPlus} from 'react-icons/im';
import {FaMinus} from 'react-icons/fa';

import testImage from './testImage/3-300x300.jpg'
import {Link} from "react-router-dom";
import ModalWindow from "../../../assets/ModalWindow/ModalWindow";
import {useDispatch, useSelector} from "react-redux";
import {addToCartProduct, DetailProduct} from "../../../../action/cartItemsAction";


const ProductsItem = ({dataProduct}) => {
    const [inputValue, setinputValue] = useState([])
    const [modalActive, setActiveModal] = useState(false)
    const [idProduct, setIdProduct] = useState()
    const cartItems = useSelector((state => state.AcsessOrder.cartItems))
    const dispatch = useDispatch()

    const AddProductInCount = (id, qty = 1) => {
        const exist = inputValue.find((x) => x.id === id);
        if (exist) {
            setinputValue(
                inputValue.map((x) =>
                    x.id === id ? {...exist, qty: Number(qty)} : x
                )
            )
        } else {
            setinputValue([...inputValue, {id: id, qty: qty}]);
        }
    }

    const RemovePlus = (id, qty = 1) => {
        const exist = inputValue.find((x) => x.id === id);
        if (exist) {
            setinputValue(
                inputValue.map((x) =>
                    x.id === id ? {...exist, qty: Number(exist.qty) + 1} : x
                )
            );
        }
    }
    const onRemoveMinus = (id) => {
        const exist = inputValue.find((x) => x.id === id);
        if (exist.qty === 1) {
            setinputValue(inputValue.filter((x) => x.id !== id));
        } else {
            setinputValue(
                inputValue.map((x) =>
                    x.id === id ? {...exist, qty: exist.qty - 1} : x
                )
            );
        }
    };

    const CountQty = (id) => {
        const exist = inputValue.find((x) => x.id === id);
        if (exist) {
            return exist.qty
        } else {
            setinputValue([...inputValue, {id: id, qty: 1}])
            return 1
        }

    }
    return (
        <>
            <div className="product" id='Products'>
                {dataProduct.products.map(x => {
                    return <div key={x._id} className="Product-item">
                        <Link onClick={() => dispatch(DetailProduct([x]))} to={`/product/${x._id}`}><img src={testImage} alt=""/></Link>
                        <div className="products-box">
                            <Link onClick={() => dispatch(DetailProduct([x]))} to={`/product/${x._id}`}><h2>{x.title}</h2></Link>
                            <p>{x.description}</p>
                            <span>${x.price}</span>
                        </div>
                        <div className="center-btn">
                            <div>
                                <span onClick={() => RemovePlus(x._id, CountQty(x._id))}
                                      className='btn_plus_border'><ImPlus
                                    id='btn_plus'/></span>
                                <input type="number" placeholder="1" min='1' max='200' value={CountQty(x._id)}
                                       onChange={(event) => AddProductInCount(x._id, event.target.value)}/>
                                <span onClick={() => onRemoveMinus(x._id)} className='btn_minus_border'><FaMinus
                                    id='btn_minus'/></span>
                            </div>
                            <button onClick={() => {
                                dispatch(addToCartProduct(x, CountQty(x._id)))
                                setActiveModal(true);
                                setIdProduct(x._id)
                            }}><BiCart/>Add to cart
                            </button>
                        </div>
                        <div/>
                    </div>
                })}
                <ModalWindow modalActive={modalActive} setActiveModal={setActiveModal}>
                    <div className="modal_title">
                        <p>Added to cart successfully. What's next?</p>
                        <span onClick={() => {
                            setActiveModal(false)
                        }}>x</span>
                    </div>
                    <div className="modal_content_container">
                        <div className="modal_image_content">
                            <img src={testImage} alt=""/>
                        </div>
                        {cartItems.map(y => idProduct === y._id
                            ?
                            <div key={y._id} className="modal_info_product_colom">
                                <div>
                                    <div className='modal_title_product_colom'>
                                        <p>{y.title}</p>
                                    </div>
                                    <div>
                                        <span className='modal_qty_product'>{y.qty} x </span>
                                        <span className='modal_price_product'>${(y.price * y.qty).toFixed(2)}</span>
                                    </div>
                                </div>
                            </div>
                            : null)}
                        <div className="modal_checkout_container">
                            {cartItems.map(b => idProduct === b._id
                                ?
                                <div key={b._id} className={"modal_checkout_content"}>
                                    <Link id='teststr' to="/checkout">checkout</Link>
                                    <p>Order</p>
                                    <span> ${cartItems.reduce((a, c) => a + c.price * c.qty, 0).toFixed(2)}</span>
                                    <p>You have product in the cart : {cartItems.length}</p>
                                    <Link onClick={()=>setActiveModal(false)} to="">Continue shopping</Link>
                                    <Link to="/cart">Cart</Link>
                                </div>
                                : null)}
                        </div>
                    </div>
                </ModalWindow>
            </div>
        </>
    );
};

export default ProductsItem;