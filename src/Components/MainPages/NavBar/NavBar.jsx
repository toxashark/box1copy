import React, {useState} from 'react';
import {Link} from 'react-router-dom'
import Slider from '@material-ui/core/Slider';


import tesImage from '../Products/Product/testImage/3-300x300.jpg'
import {useDispatch, useSelector} from "react-redux";
import {RemoveProduct} from "../../../action/cartItemsAction";


const NavBar = () => {
    const [value, setValue] = useState([5, 25])
    const [one, two] = value
    const cartItems = useSelector((state => state.AcsessOrder.cartItems))
    const dispatch = useDispatch()
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className="NavBar-container">
            <div className="Navbar-cart">
                <div className="Navbar-cart-title">
                    Cart
                </div>
                <div className="NavBar-cart-text">
                    <div>
                        {cartItems.length === 0 ? <div className="navBar_cart_content_text"> <p>No Product in the cart.</p></div> :
                            <div className='active-cart-text'>You have {cartItems.length} in the cart:</div>}
                    </div>
                    <div className="cart">
                        <ul className="items_Product">
                            {cartItems.map(item => (
                                <li key={item._id}>
                                    <div><img src={tesImage} alt=''/></div>
                                    <div className='cart-content-price'>
                                        <div className="cart-Title">{item.title}</div>
                                        <div className="right-price">
                                            {item.qty} x <span>${(item.price * item.qty).toFixed(2)}</span>
                                        </div>
                                    </div>
                                    <span id="style-deleted_product" onClick={()=>dispatch(RemoveProduct(item))}>X</span>
                                </li>
                            ))}
                        </ul>
                        {cartItems.length !== 0 && (
                            <div className='SubTotal'>
                                Subtotal: ${cartItems.reduce((a, c) => a + c.price * c.qty, 0).toFixed(2)}
                                <div className="cart-button">
                                    <Link to="/cart"><button id="View-cart">View Cart</button></Link>
                                    <Link to="/checkout"><button id="chekout">CHEKOUT</button></Link>
                                </div>
                            </div>
                        )}

                    </div>
                </div>
            </div>
            <div className="Navbar-cart">
                <div className="Navbar-cart-title">
                    Categories
                </div>
                <div className="NavBar-cart-text">
                    <p>Categories - 1</p>
                    <p>Categories - 2</p>
                    <p>Categories - 3</p>
                </div>
            </div>
            <div className="Navbar-cart">
                <div className="Navbar-cart-title">
                    Filter
                </div>
                <div className="NavBar-cart-text">
                    <div className="price_slider">
                        <Slider
                            min={0}
                            step={1}
                            max={50}
                            value={value}
                            onChange={handleChange}
                            valueLabelDisplay="auto"
                            aria-labelledby="range-slider"
                        />
                        <div className="filter_text">
                            <p>Price: ${one}<span> - </span>${two}</p>
                        </div>
                        <div className='filter_button'>
                            <button>Filter</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default NavBar;