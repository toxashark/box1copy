import React, {useState,useEffect} from 'react';
import {Link} from 'react-router-dom'
import MaratIcon from './icon/logo.png'
import {ItemsHome} from './ItemsMenuDropDown/items'
import { FaBars, FaPhoneAlt, FaTelegram} from 'react-icons/fa';
import { TiTimes, TiContacts } from 'react-icons/ti';
import { BsHouseDoor, BsSearch, BsInfoCircle} from 'react-icons/bs';
import { HiOutlineShoppingBag } from 'react-icons/hi';
import {BiCart} from 'react-icons/bi';
import {SiMessenger,SiViber} from 'react-icons/si';
import {GoChevronRight, GoChevronDown} from 'react-icons/go';
import ModalWindow from "../assets/ModalWindow/ModalWindow";
import {useSelector} from "react-redux";




const Header = () => {
    const [menuActive, setMenuActive] = useState(false)
    const [click, setClick] = useState(false)
    const [dropDown, setDropDown] = useState(false)
    const [searchActive, setSearchActive] = useState(false)
    const [searchValue, setSearchValu] = useState('')
    const [headerStyle, setHeaderStyl] = useState("container_header")
    const [modalActive, setActiveModal] = useState(false)
    const handleClick = () => setClick(false)
    const cartItems = useSelector((state => state.AcsessOrder.cartItems))
    const onMouseEnter = () => {
        if (window.innerWidth < 960) {
            setDropDown(false)
        } else {
            setDropDown(true)
        }
    };
    const onMouseLeave = () => {
        if (window.innerWidth < 960) {
            setDropDown(false)
        } else {
            setDropDown(false)
        }
    };
    const DropDown = ({ItemsHome}) => {
        return (
            <>
                    <ul onClick={handleClick} className={click ? "dropDown-menu clicked" : "dropDown-menu"}>
                        {ItemsHome.map((item, index) => {
                            return (
                                <li key={index}>
                                    <Link className={item.cName} to={item.path} onClick={() => {
                                        setClick(false)
                                    }}>
                                        {item.title}
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
            </>
        )
    }
    const listenScrollEvent = (event) => {
        if (window.scrollY < 1 && window.innerWidth < 501) {
           setHeaderStyl("container_header")
            setSearchActive(false)
        } else if (window.scrollY > 80 && window.innerWidth < 501) {
            setHeaderStyl("container_header_for_mobile")
            setSearchActive(true)
        }
    }
    useEffect(() => {
        window.addEventListener('scroll', listenScrollEvent);
        return () =>
            window.removeEventListener('scroll', listenScrollEvent);
    }, []);

    return (
        <div className={headerStyle}>
            <header>
                <div className="logo">
                    <a target="_blank" rel='noreferrer' href="https://themarat.com/en/website-development.html"><img src={MaratIcon} alt=""/></a>
                </div>
                    <ul className={menuActive ? "nav-menu active" : "nav-menu"}>
                        <div  onClick={()=>setMenuActive(!menuActive)}>
                            {menuActive ? <div className="menu_close_svg"><img src={MaratIcon} alt="1"/><TiTimes/></div> : null}
                        </div>
                        {menuActive ? <BsHouseDoor className="nav-item_icon"/> : null}
                        <li className="nav-item" onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>

                            <Link to="/">
                                Home {dropDown ? <GoChevronDown/> : <GoChevronRight/>}
                            </Link>
                            {dropDown && <DropDown ItemsHome={ItemsHome}/>}
                        </li>
                        {menuActive ? <BsInfoCircle className="nav-item_icon"/> : null}
                        <li className="nav-item"><a href='#AboutUs'>About Us <GoChevronRight/></a></li>
                        {menuActive ? <HiOutlineShoppingBag className="nav-item_icon"/> : null}
                        <li className="nav-item"><a href="#Products">Products <GoChevronRight/></a></li>
                        {menuActive ? <TiContacts className="nav-item_icon"/> : null}
                        <li className="nav-item"><a href="#ServiceOrder">Contacts</a></li>
                            {menuActive ?
                                <div onClick={()=>{setActiveModal(true)}} className="sidebar_contact">
                                    <span><FaPhoneAlt/>0-800-999-999<GoChevronDown className='sidebar_contact_chevr'/></span>
                                </div>
                                :
                                null}
                            <ModalWindow modalActive={modalActive} setActiveModal={setActiveModal}>
                                <div className="modalwindow_mobile">
                                    <ul>
                                        <li><SiMessenger className="mes_teleg"/><a href="!#">Messenger</a></li>
                                        <li><SiViber className="viber"/><a href="viber://chat?number=%2B4957777777">Viber</a></li>
                                        <li><FaTelegram className="mes_teleg"/>
                                            <a href="tg://resolve?domain=MindAdmin_95">Telegram</a></li>
                                        <li>
                                            <FaPhoneAlt className="phone_mobile"/>
                                            <div>
                                                <p><a href="tel:+74955555555">+7(495) 555-55-55</a></p>
                                                <span>With 8.00AM - 21.00PM(Mon-Sun)<br/></span>
                                                <span>Free as Ukrain</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </ModalWindow>
                    </ul>
                    <div onClick={()=>{setMenuActive(false)}} className={menuActive ? "bacground_menu_active"  : null}/>
                <div className="search-icon">
                    <Link to='#'>
                        {!searchActive
                            ?
                            <BsSearch onClick={() => {
                                setSearchActive(!searchActive)
                            }}/>
                            :
                            <div className="input_search_active">
                                <input value={searchValue} onChange={(event) => {
                                    setSearchValu(event.target.value)
                                }} placeholder="Search" type="text"/>
                                <BsSearch onClick={() => {
                                    setSearchActive(!searchActive)
                                }}/>
                            </div>}
                    </Link>
                </div>
                <div className="cart-icon">
                    <span>{cartItems.length}</span>
                    <Link to='/cart'>
                        <BiCart/>
                    </Link>
                </div>
                <div className="menu" onClick={()=>setMenuActive(!menuActive)}>
                    <FaBars/>
                </div>
            </header>
        </div>
    );
};

export default Header;