import React from 'react';
import withBreadcrumbs from "react-router-breadcrumbs-hoc";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

const VolnaStyle = () => {
    const detailProduct = useSelector((state => state.AcsessOrder.detailProduct))
    const detailProductItems = detailProduct[0]
    const breadcrumbid = detailProductItems._id
    const breadcrumbTitle = detailProductItems.title
    const userNamesById = {}
    userNamesById[breadcrumbid] = breadcrumbTitle


    const DynamicUserBreadcrumb = ({match}) => (
        <span className="last_link_breeadcrumb">{userNamesById[match.params.userId]}</span>);
    const routes = [{path: "/product/:userId", breadcrumb: DynamicUserBreadcrumb},
        {path: "/product", breadcrumb: null}];
    const Breadcrumbs = withBreadcrumbs(routes)(({breadcrumbs}) => (
        <div className="bredcrumb_product">
            {breadcrumbs.map(({match, breadcrumb}) => (
                <span key={match.url}>
                    <span className="style_slash"> /</span>
        <Link to={match.url}>{breadcrumb}</Link>
      </span>
            ))}
        </div>
    ));
    const ForProduct = () => {
        return (
            <div className='breacrumb_title'>
                {detailProduct.map(y => {
                    return (
                        <div key={y._id}>{y.title}</div>
                    )
                })}
                <Breadcrumbs/>
            </div>
        )
    }
    return (
        <div className="Main_cintainer_volna">
            <div className='cart_background'>
                {detailProduct ? <ForProduct/> : null}
            </div>

            <div className="volna"/>
        </div>
    );
};
export default VolnaStyle;