import React from 'react';
import withBreadcrumbs from "react-router-breadcrumbs-hoc";
import {Link} from "react-router-dom";

const VolnaStyleCart = ({detailProduct}) => {

    const DynamicUserBreadcrumb = ({match}) => (
        <span className="last_link_breeadcrumb">Cart</span>
    );

    const routes = [{path: "/cart", breadcrumb: DynamicUserBreadcrumb}];
    const Breadcrumbs = withBreadcrumbs(routes)(({breadcrumbs}) => (
        <div className="breadcrumbCart">
            {breadcrumbs.map(({match, breadcrumb}) => (
                <span key={match.url}>
        <Link to={match.url}>
          /{breadcrumb}
        </Link>
      </span>
            ))}
        </div>
    ));
    const ForProduct = () => {
        return (
            <div className='breacrumb_title'>
                
                <div className="title_cart">Cart</div>
                <Breadcrumbs/>
            </div>
        )
    }


    return (
        <div className="Main_cintainer_volna">
            <div className='cart_background'>
                <ForProduct/>
            </div>
            <div className="volna"/>
        </div>
    );
};

export default VolnaStyleCart;