import React from 'react';

const ModalWindow = ({modalActive,setActiveModal,children}) => {
    return (
        <div className={modalActive ? "modal active" : "modal"} onClick={()=>setActiveModal(false)}>
            <div className={modalActive ? "content-modal active" : "content-modal"} onClick={e=>e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
};
export default ModalWindow;