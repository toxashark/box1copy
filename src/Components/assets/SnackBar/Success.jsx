import React,{useEffect,useRef} from 'react';
import {MdDoneAll} from 'react-icons/md';
import {BsX} from 'react-icons/bs';


const SuccessSnackBar = ({activeSnack, setActiveSnack, children}) => {

    const timer = useRef();
    const handleButtonClick = () => {
        if (activeSnack) {
            timer.current = window.setTimeout(() => {
                setActiveSnack(false)
            }, 5000);
        }
    };
    useEffect(() => {
        handleButtonClick()
        return () => {
            clearTimeout(timer.current);
        };
    }, [activeSnack]);
    return (
        <>
            <div className={activeSnack ? "snack active" : "snack"}>
                    <div className={activeSnack ? "content-snack active" : "content-snack"}>
                        <MdDoneAll onClick={()=>{handleButtonClick()}} className="done_snackbar"/>
                        <div className="text-content-snackbar">
                            {children}
                        </div>
                        <BsX onClick={()=>{setActiveSnack(false)}} className="cross-snackbar"/>
                    </div>
            </div>
        </>
    );
};
export default SuccessSnackBar;