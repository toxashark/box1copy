import React,{useState,useEffect,useRef} from 'react';
import {MdErrorOutline} from 'react-icons/md';
import {BsX} from 'react-icons/bs';


const ErrorSnackBar = ({activeError, setActiveError, children}) => {

    const timer = useRef();
    const handleButtonClick = () => {
        if (activeError) {
            timer.current = window.setTimeout(() => {
                setActiveError(false)
            }, 5000);
        }
    };
    useEffect(() => {
        handleButtonClick()
        return () => {
            clearTimeout(timer.current);
        };
    }, [activeError]);
    console.log(activeError)
    return (
        <>
            <div className={activeError ? "snack active" : "snack"}>
                <div className={activeError ? "content-snack-error active" : "content-snack-error"}>
                    <MdErrorOutline  className="done_snackbar"/>
                    <div className="text-content-snackbar">
                        {children}
                    </div>
                    <BsX onClick={()=>{setActiveError(false)}} className="cross-snackbar"/>
                </div>
            </div>
        </>
    );
};

export default ErrorSnackBar;