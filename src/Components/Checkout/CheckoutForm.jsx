import * as Yup from "yup";
import {ErrorMessage, Field, Form, Formik, getIn} from "formik";
import React, {useState} from "react";
import SuccessSnackBar from "../assets/SnackBar/Success";
import {useDispatch, useSelector} from "react-redux";
import {Order} from "../../action/cartItemsAction";

const CheckoutForm = () => {
    const [activeSnack, setActiveSnack] = useState(false)
    const cartItems = useSelector((state => state.AcsessOrder.cartItems))
    const orderInfo = useSelector((state => state.AcsessOrder.order))
    const dispatch = useDispatch()

    const getStyles =(errors, fieldName) => {
        if (getIn(errors, fieldName)) {
            return {
                border: '1px solid red'
            }
        }
    }
    const StyleRequired = (props) => {
        return (
            <>
                <div className="ErrorMassage">
                    {props.children}
                </div>
            </>
        )
    }
    const initialValues = {
        FirstName: '',
        LastName: '',
        Company:'',
        HouseNumber:'',
        street:'',
        city:'',
        country:'',
        code:'',
        phone:'',
        email:'',
        Order: ''
    }
    const onSubmit = (values, onSubmitProps) => {
        onSubmitProps.resetForm()
        dispatch(Order({values,cartItems}))
        setActiveSnack(true)
        console.log(orderInfo)
    }
    const validationSchema = Yup.object({
        FirstName: Yup.string().required(' '),
        LastName: Yup.string().required(' '),
        Company:Yup.string().required(' '),
        HouseNumber:Yup.string().required(' '),
        street:Yup.string().required(' '),
        city:Yup.string().required(' '),
        country:Yup.string().required(' '),
        code:Yup.string().required(' '),
        phone:Yup.string().required(' '),
        email:Yup.string().required(' '),
        Order: Yup.string().required(' '),
    })
    return (
        <div>
            <Formik validateOnChange={false} initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                {formik => {
                    return (
                        <Form>
                            <div className="container-checkout">
                                <div className="chekout_container_form">
                                    <div className="checkout_container_1">
                                        <div className="chockout_title_form">
                                            Billing details
                                        </div>
                                        <div className="checkout_name">
                                            <div className="checkout_colom">
                                                <label htmlFor="First Name">First<span>*</span></label>
                                                <Field style={getStyles(formik.errors, 'FirstName')}
                                                    type="FirstName"
                                                    id="FirstName"
                                                    name="FirstName"
                                                />
                                                <ErrorMessage name='FirstName' component={StyleRequired}/>
                                            </div>
                                            <div className="checkout_colom checkout_lastName">
                                                <label htmlFor="LastName">LastName<span>*</span></label>
                                                <Field style={getStyles(formik.errors, 'LastName')}
                                                    type="LastName"
                                                    id="LastName"
                                                    name="LastName"
                                                />
                                                <ErrorMessage name='LastName' component={StyleRequired}/>
                                            </div>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="Company">Company nam(optional)<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'Company')}
                                                type="Company"
                                                id="Company"
                                                name="Company"
                                            />
                                            <ErrorMessage name='Company' component={StyleRequired}/>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="HouseNumber">Street address<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'HouseNumber')}
                                                type="adres"
                                                id="HouseNumber"
                                                name="HouseNumber"
                                                placeholder="House number adn street name"
                                            />
                                            <ErrorMessage name='HouseNumber' component={StyleRequired}/>
                                        </div>
                                        <div id="margin_street" className="checkout_colom checkout_Margin">
                                            <label htmlFor="street"></label>
                                            <Field style={getStyles(formik.errors, 'street')}
                                                type="adres"
                                                id="street"
                                                name="street"
                                                placeholder="Apartmnt, suit unit etc.(optional)"
                                            />
                                            <ErrorMessage name='street' component={StyleRequired}/>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="city">Town/City<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'city')}
                                                type="adres"
                                                id="city"
                                                name="city"
                                            />
                                            <ErrorMessage name='city' component={StyleRequired}/>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="country">State/Country<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'country')}
                                                type="country"
                                                id="country"
                                                name="country"
                                            />
                                            <ErrorMessage name='country' component={StyleRequired}/>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="code">Postcode/ZIP<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'code')}
                                                type="code"
                                                id="code"
                                                name="code"
                                            />
                                            <ErrorMessage name='code' component={StyleRequired}/>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="phone">Phone<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'phone')}
                                                type="phone"
                                                id="phone"
                                                name="phone"
                                            />
                                            <ErrorMessage name='phone' component={StyleRequired}/>
                                        </div>
                                        <div className="checkout_colom checkout_Margin">
                                            <label htmlFor="email">Email address<span>*</span></label>
                                            <Field style={getStyles(formik.errors, 'email')}
                                                type="email"
                                                id="email"
                                                name="email"
                                            />
                                            <ErrorMessage name='email' component={StyleRequired}/>
                                        </div>
                                    </div>
                                    <div className="checkout_container_2">
                                        <div className="chockout_title_form">
                                            Additional information
                                        </div>
                                        <div className="checkout_colom">
                                            <label htmlFor="Order">Order notes(optional)</label>
                                            <Field style={getStyles(formik.errors, 'Order')}
                                                   vertical="1"
                                                   as='textarea'
                                                   type="Order"
                                                   id="Order"
                                                   name="Order"
                                                   placeholder='Notes about order, e.g. special notes for delivery'
                                            />
                                            <ErrorMessage name='Order' component={StyleRequired}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="checkout_form_order_container">
                                    <span>You order</span>
                                    <div className="checkout_form_order_header">
                                        <div>Product</div>
                                        <div>Total</div>
                                    </div>
                                    <div className="checkout_form_border">
                                        {cartItems.map(x=>{
                                            return (
                                                <div key={x._id} className="checkout_form_content" >
                                                    <div className="checkout_form_content_title">{x.title} x {x.qty}</div>
                                                    <div className="checkout_form_content_price">${x.price}</div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                    <div className="checkout_form_total">
                                        <div className='checkout_total'>
                                            Total:
                                        </div>
                                        <div className="checkout_total_price">
                                            ${cartItems.reduce((a, c) => a + c.price * c.qty, 0).toFixed(2)}
                                        </div>
                                    </div>
                                </div>
                                <div className="checkout_total_button">
                                    <button type="submit">Make Order</button>
                                </div>
                            </div>
                        </Form>
                    )
                }}
            </Formik>
            <SuccessSnackBar activeSnack={activeSnack} setActiveSnack={setActiveSnack}>
                    <div>
                        <p>Success</p>
                    </div>
            </SuccessSnackBar>
        </div>
    )
}
export default CheckoutForm