import React from 'react';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import {Link} from "react-router-dom";
import VolnaStyleCart from "../assets/StyleVolna/VolaCart";
import CheckoutForm from "./CheckoutForm";
import {useSelector} from "react-redux";


const Checkout = () => {
    const cartItems = useSelector((state => state.AcsessOrder.cartItems))
    return (
        <div>
            <Header/>
            che
            <VolnaStyleCart/>
            {cartItems.length === 0 ? <div className="container-cart-empty">
                <div>You cart is currently empty.</div>
                <Link className="style_button_cart" to='/'>Return to shop</Link>
            </div> :  <CheckoutForm/>}
            <Footer/>
        </div>
    );
};
export default Checkout;