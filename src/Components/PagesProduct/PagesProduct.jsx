import React, {useEffect} from 'react';
import {Redirect} from 'react-router-dom'

import Header from "../Header/Header";
import VolnaStyle from "../assets/StyleVolna/volna";
import NavBar from "../MainPages/NavBar/NavBar";
import Product from './DetailProduct/Product'
import Footer from '../Footer/Footer'
import {useSelector} from "react-redux";



const PagesProduct = () => {
    const detailProduct = useSelector((state => state.AcsessOrder.detailProduct))
    const up = () => {
        const top = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
        if (top > 0) {
            window.scrollBy(0, -1000000);
        } else
        return false;
    }

    useEffect(() => {
        up()
    }, [])
    return (
        <>
            {detailProduct.length === 0 ? <Redirect to="/" /> : <div>
                <Header/>
                <VolnaStyle/>
                <div className='MainPage-container_detail'>
                        {window.innerWidth < 999 ? null : <div><NavBar/></div>}
                    <div>
                    <Product/>
                    </div>
                </div>
                <Footer/>
            </div>}

        </>

    );
};

export default PagesProduct;