import React from 'react'
import {useSelector} from "react-redux";

const Description = () => {
    const detailProduct = useSelector((state => state.AcsessOrder.detailProduct))
    return (
        <div>
            <div className="reviews_title_description">
                        Description
                    </div>
            {detailProduct.map(y => {
                return (
                    <div key={y._id} className="reviews_text_description">
                        {y.description_product}
                    </div>
                )
            })}
        </div>
    )
}

export default Description