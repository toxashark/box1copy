import React, {useState, useEffect} from 'react'
import {Formik, Form, Field, ErrorMessage, getIn} from 'formik'
import * as Yup from 'yup'
import Rating from '@material-ui/lab/Rating';

import { BiUser } from 'react-icons/bi';
import TedtImage from '../../MainPages/Products/Product/testImage/3-300x300.jpg'
import Description from "./Description";
import {useDispatch, useSelector} from "react-redux";
import {addToCartProduct} from "../../../action/cartItemsAction";

const Product = () => {
    const [FlagActive, setFlagActive] = useState(true)
    const [valueReting, setValueReting] = useState(2)
    const dispatch = useDispatch()
    const detailProduct = useSelector((state => state.AcsessOrder.detailProduct))

    //Тестовый коментарий сотавлять для продукта
    const [valueReview, setValueReview] = useState([])
    const [activetest, setActivetest] = useState(false)

    function getStyles(errors, fieldName) {
        if (getIn(errors, fieldName)) {
            return {
                border: '1px solid red'
            }
        }
    }

    const StyleRequired = (props) => {
        return (
            <>
                <div className="ErrorMassage">
                    {props.children}
                </div>

            </>

        )
    }

    const ReviewForm = () => {

        const initialValues = {
            review: '',
            name: '',
            email: ''
        }

        const onSubmit = (values, onSubmitProps) => {
            onSubmitProps.resetForm()
            setValueReview([{values, valueReting}])
            setValueReting(2)
            console.log(valueReview.length)
            console.log(valueReview)
        }
        const validationSchema = Yup.object({
            review: Yup.string().required('Поле не может быть пустым'),
            name: Yup.string().required('Поле не может быть пустым'),
            email: Yup.string().required('Поле не может быть пустым'),
        })
        return (
            <div>
                <Formik validateOnBlur={false} validateOnChange={false} initialValues={initialValues}
                        onSubmit={onSubmit} validationSchema={validationSchema}>
                    {formik => {
                        return (
                            <Form>
                                <div className="review_container_form_">
                                    <div>
                                        <div className="review_title_form">
                                            <p>You review*</p>
                                        </div>
                                        <label htmlFor="review"></label>
                                        <Field style={getStyles(formik.errors, 'review')}
                                               vertical="1"
                                               as='textarea'
                                               type="review"
                                               id="review"
                                               name="review"
                                        />
                                        <ErrorMessage name='review' component={StyleRequired}/>
                                    </div>
                                    <div>
                                        <div className="review_title_form">
                                            <p>Name*</p>
                                        </div>
                                        <label htmlFor="name"></label>
                                        <Field style={getStyles(formik.errors, 'name')}
                                               type="name"
                                               id="name"
                                               name="name"
                                        />
                                        <ErrorMessage name='name' component={StyleRequired}/>
                                    </div>
                                    <div>
                                        <div className="review_title_form">
                                            <p>Email*</p>
                                        </div>
                                        <label htmlFor="email"></label>
                                        <Field style={getStyles(formik.errors, 'email')}
                                               type="email"
                                               name="email"
                                        />
                                        <ErrorMessage name='email' component={StyleRequired}/>
                                    </div>
                                    <div className="review_button_form">
                                        <button type="submit">Submit</button>
                                    </div>
                                </div>
                            </Form>
                        )
                    }}
                </Formik>
            </div>
        )
    }
    useEffect(() => {
        if (valueReview.length === 0) {
            return null
        } else {
            setActivetest(true)
        }
    }, [valueReview])
    return (
        <div>
            {detailProduct.map(x => {
                return (
                    <div key={x._id} className="detailProduct_container">
                        <div>
                            <img src={TedtImage} alt=""/>
                        </div>

                        <div className='detail_container'>
                            <div className="title_detail_product">
                                {x.title}
                            </div>
                            <div className='price_detail_product'>
                                ${x.price}
                            </div>
                            <div className='descroption_detail_product'>
                                {x.description}
                            </div>
                            <div className="button_detail_product">
                                <button onClick={() => dispatch(addToCartProduct(x))}>Add to cart</button>
                            </div>
                            <div className="category_detail_product">
                                <div>
                                    Category: <span id="color_tags">Water</span>
                                </div>
                                <div>
                                    Tag: <span id="color_tags">pack</span>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })}
            <div className="reviews_containers">
                <div className="reviews_title_container">
                    <div onClick={() => setFlagActive(true)}
                         className={FlagActive ? "reviews_title" : "reviews_title active"}>
                        Description
                    </div>
                    <div onClick={() => setFlagActive(false)}
                         className={FlagActive ? "reviews_title active" : "reviews_title"}>
                        Reviews(0)
                    </div>
                </div>
                <div className="reviews_description">

                    <div>
                        {FlagActive
                            ?
                            <Description/>
                            :
                            <div>
                                <div className="reviews_title_description">
                                    Reviews
                                    <div className="reviews_text_form_description">
                                        {activetest
                                            ?
                                            <div className='review_users_container'>{valueReview.map(x => {
                                                return (
                                                    <div className="review_users_content" key={x.valueReting}>
                                                        <div className="review_users_img">
                                                            <BiUser/>
                                                        </div>
                                                        <div className="review_users_title_container">
                                                            <div>
                                                                <div  className="review_users_title_title">
                                                                    <span>You review is awaiting approval</span>
                                                                </div>
                                                                <div className="review_users_reviews">
                                                                    <span>{x.values.review}</span>
                                                                </div>
                                                            </div>
                                                            <Rating size="small" name="read-only" value={x.valueReting} readOnly/>
                                                        </div>
                                                    </div>
                                                )
                                            })}</div>
                                            :
                                            <div>
                                                <div id="title-content-reviw">
                                                    There are no reviews yet.
                                                </div>
                                                <div id="review-text-form_">
                                                    Be the first to review “Three bottles of mineral water”
                                                </div>
                                            </div>}

                                        <div id="title_main_form-reviw">
                                            Your email address will not be published. Required fields are marked *
                                        </div>
                                        <div className="Rating_container">
                                            <p>You Rating</p>
                                            <Rating name="simple-controlled" value={valueReting}
                                                    onChange={(event, newValue) => {
                                                        setValueReting(newValue)
                                                    }}/>
                                        </div>
                                    </div>
                                    <ReviewForm/>
                                </div>
                            </div>}
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Product