import React from 'react';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import {ImPlus} from 'react-icons/im';
import {FaMinus} from 'react-icons/fa';
import testImage from "../MainPages/Products/Product/testImage/3-300x300.jpg";
import {Link} from "react-router-dom";
import VolnaStyleCart from "../assets/StyleVolna/VolaCart";
import CartItemsProduct from './cartitems'
import {useDispatch, useSelector} from "react-redux";
import {addToCartProduct, DetailProduct, RemoveMinus, RemoveProduct} from "../../action/cartItemsAction";


const Cart = () => {
    const cartItems = useSelector((state => state.AcsessOrder.cartItems))
    const dispatch = useDispatch()

    return (
        <div>
            <Header/>
            <VolnaStyleCart/>
            {cartItems.length === 0
                ?
                <div className="container-cart-empty">
                    <div>You cart is currently empty.</div>
                    <Link className="style_button_cart" to='/'>Return to shop</Link>
                </div>
                :
                window.innerWidth < 850
                    ?
                    <div className="Mobile_container_style">
                        {cartItems.map(x => {
                            return (
                                <>
                                    <div className="main_cart_mobile" key={x._id}>
                                        <CartItemsProduct/>
                                        <div className="cart_content_mobile">
                                            <div className="deleted_product_mobile"><span id="deleted_mobile"
                                                                                          onClick={() => dispatch(RemoveProduct(x))}>X</span>
                                            </div>

                                            <div><Link onClick={() => dispatch(DetailProduct([x]))}
                                                       to={`/product/${x._id}`}>{x.title}</Link></div>
                                            <div className="Style_Mobile_price">${x.price}</div>
                                            <div>
                                                <div className="oder-chaged-qty_mobile">
                                                    <span onClick={() => dispatch(addToCartProduct(x))}><ImPlus/></span>
                                                    <p>{x.qty}</p>
                                                    <span onClick={() => dispatch(RemoveMinus(x))}><FaMinus/></span>
                                                </div>
                                            </div>
                                            <div
                                                className="order-price order_price_style Style_Mobile_price">${(x.price * x.qty).toFixed(2)}</div>
                                        </div>
                                    </div>
                                </>
                            )
                        })}
                        <div className="container_total_mobile">
                            <div>
                                Total:
                            </div>
                            <div>
                                ${cartItems.reduce((a, c) => a + c.price * c.qty, 0).toFixed(2)}
                            </div>
                        </div>
                        <div className="container_button_cart">
                            <Link className="style_button_cart" to='/checkout'>Proceed to checkout</Link>
                        </div>

                    </div>
                    :
                    <div className="main-cart-container">
                        <div className="main_cart_items">
                            <div>title</div>
                            <div>price</div>
                            <div>Quantity</div>
                            <div>Total</div>
                        </div>
                        {cartItems.map(x => {
                            return <div key={x._id} className="main_cart_items_order">
                                <div className='order-images'><span id="deleted_order"
                                                                    onClick={() => dispatch(RemoveProduct(x))}>x</span><Link
                                    onClick={() => dispatch(DetailProduct([x]))} to={`/product/${x._id}`}><img
                                    src={testImage} alt=""/></Link></div>
                                <div className="order-title-border"><Link onClick={() => dispatch(DetailProduct([x]))}
                                                                          to={`/product/${x._id}`}>{x.title}</Link>
                                </div>
                                <div className="order-price">${x.price}</div>
                                <div className="container_oder-chaged-qty">
                                    <div className="oder-chaged-qty">
                                        <span onClick={() => dispatch(addToCartProduct(x))}><ImPlus/></span>
                                        <p>{x.qty}</p>
                                        <span onClick={() => dispatch(RemoveMinus(x))}><FaMinus/></span>
                                    </div>
                                </div>
                                <div className="order-price order_price_style">${(x.price * x.qty).toFixed(2)}</div>
                            </div>
                        })}
                        <div className="subTotal-order">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div className=" order--total-price">
                                Total: ${cartItems.reduce((a, c) => a + c.price * c.qty, 0).toFixed(2)}
                                <Link className="style_button_cart" to='/checkout'>CHECKOUT</Link>
                            </div>
                        </div>
                    </div>
            }
            <Footer/>
        </div>
    );
};
export default Cart;