import React, {useEffect, useState} from 'react';

import Bottle from './images/slider-dark-bottle.webp'


const Slider = () => {
    const [click, setClick] = useState(false)

    useEffect(() => {
        const ggg = () =>{
            if(window.innerWidth < 960){

            }else{
                setClick(!click)
            }
        };
        const interval = setInterval(() => {
            ggg()

        }, 5000)

        return () => clearInterval(interval)
    }, [click])


    return (
        <div className='slider-container'>
            <div className={click ? "ffff" : "ffff active"}>
                <div className="container-content">
                    <div className='title'>
                        Mineral Water
                        <p>for Every Day</p>
                    </div>
                    <div className="text-content">
                        Our delivery service employs more than 100 professional couries.
                        We will delivr water to your home
                    </div>
                    <div className="Link-slider">
                        <a href="!#" className="Link-slider1">Read More</a>
                        <a href="!#" className="Link-slider2">Make Order</a>
                    </div>
                </div>
                <div className="bottle">
                    <img src={Bottle} alt=""/>
                </div>
            </div>
            <div className="bubl">
                <div className="bubl1"></div>
                <div className="bubl2"></div>
                <div className="bubl3"></div>
            </div>
        </div>

    );
};

export default Slider;