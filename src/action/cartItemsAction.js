import {
    ACSESS_ORDER,
    CART_ITEMS,
    CART_ITEMS_CHANGE_MINUS,
    CART_ITEMS_CHANGE_QTY,
    CART_ITEMS_REMOVE,
    DETAIL_PRODUCT
} from "../type/type";

//Добовление продукта в корзину
export const addToCartProduct = (product, qty = 1) => (dispatch, getState) => {
    const exist = getState().AcsessOrder.cartItems.find((x) => x._id === product._id);
    if (exist) {
        dispatch({type: CART_ITEMS_CHANGE_QTY, product: {...exist}, qty: qty})
    } else {
        dispatch({type: CART_ITEMS, product: {...product, qty}})
    }
};
//Удаление Товара из корзины
export const RemoveProduct = (product) => (dispatch) => {
    dispatch({type: CART_ITEMS_REMOVE, product: {...product}})
}

export const RemoveMinus = (product) => (dispatch, getState) => {
    const payload = getState().AcsessOrder.cartItems.find((x) => x._id === product._id);
    if (payload.qty === 1) {
        dispatch({type: CART_ITEMS_REMOVE, product: {...product}})
    } else {
        dispatch({type: CART_ITEMS_CHANGE_MINUS, product: {...payload}})
    }
}


export const Order = (order) => ({type: ACSESS_ORDER, order})
export const DetailProduct = (detailProduct) => ({type: DETAIL_PRODUCT, detailProduct})